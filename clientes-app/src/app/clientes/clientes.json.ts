import { Cliente } from "../models/cliente";

export const CLIENTES: Cliente[] = [
    {id: 1, nombre: 'brandon', apellido: 'mendoza', email: 'brandonalex379@gmail.com', createAt: '2020-05-24'},
    {id: 2, nombre: 'jorge', apellido: 'salinas', email: 'jsalinas@gmail.com', createAt: '2020-05-24'},
    {id: 3, nombre: 'pedro', apellido: 'guzman', email: 'pguzman@gmail.com', createAt: '2020-05-24'},
    {id: 4, nombre: 'juan', apellido: 'gonzales', email: 'jgonzeles@gmail.com', createAt: '2020-05-24'},
    {id: 5, nombre: 'jhon', apellido: 'Doe', email: 'jDoe@gmail.com', createAt: '2020-05-24'},
    {id: 6, nombre: 'omar', apellido: 'perez', email: 'omarP@gmail.com', createAt: '2020-05-24'},
    {id: 7, nombre: 'nicolas', apellido: 'prieto', email: 'nprieto@gmail.com', createAt: '2020-05-24'},
    {id: 8, nombre: 'alma', apellido: 'smith', email: 'almaS@gmail.com', createAt: '2020-05-24'},
    {id: 9, nombre: 'luisa', apellido: 'zafra', email: 'lzafra@gmail.com', createAt: '2020-05-24'},
    {id: 10, nombre: 'daniela', apellido: 'sanchez', email: 'danielaS@gmail.com', createAt: '2020-05-24'},
  ];
